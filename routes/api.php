<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/create-token','TokenController@createToken');

Route::post('/login','UserController@login');

Route::post('/register','UserController@registerUser');	

Route::post('/verify-user-otp','UserController@verifyUserOtp');	

Route::post('/sendEmail','UserController@sendTestEmail');

Route::get('/practice-code','UserController@practiceCode');	

Route::group(['middleware' => ['web','auth:api']], function()
{
   Route::post('/update-user-profile','UserController@updateProfileInfo');

   Route::group(['middleware' => ['checkRole']], function()
  {
   Route::post('/create-news-article','ArticleController@addNewArticle');
  });

   Route::post('/comment-reply','ArticleController@addCommentReply');

   Route::post('/create-article-comments','ArticleController@addComments');

   Route::get('/article-comments/{article_id}','ArticleController@getArticleComments');

   Route::get('/article-likes/{article_id}','ArticleController@addArticleLikes');

   Route::get('/getArticles','ArticleController@getArticles');


});


Route::get('/getArticle','ArticleController@getArticleWithComments');