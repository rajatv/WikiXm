<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('gender',['Male','Female']);
            $table->date('date_of_birth');
            $table->integer('age');
            $table->integer('country_id');
            $table->string('city');
            $table->string('personal_history',1000);
            $table->string('likes_dislikes',1000);
            $table->string('likes_dislikes_about_town',1000);
            $table->string('goals',1000);
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
