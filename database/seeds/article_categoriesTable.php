<?php

use Illuminate\Database\Seeder;

class article_categoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$categoriesArray = array(0 => 'Local',1 => 'Sports',2 => 'Business',3 => 'Opinion',4 => 'Health',5 => 'Education',6 => 'Entertainment',7 => 'Politics');

    	foreach($categoriesArray as $category)
    	{
			DB::table('article_category')->insert([
            		'name' => $category,
        			]);
    	}

    }
}
