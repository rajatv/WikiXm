-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: WikiXm
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wiki_article_category`
--

DROP TABLE IF EXISTS `wiki_article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_article_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_article_category`
--

LOCK TABLES `wiki_article_category` WRITE;
/*!40000 ALTER TABLE `wiki_article_category` DISABLE KEYS */;
INSERT INTO `wiki_article_category` VALUES (1,'Local',NULL,NULL),(2,'Sports',NULL,NULL),(3,'Business',NULL,NULL),(4,'Opinion',NULL,NULL),(5,'Health',NULL,NULL),(6,'Education',NULL,NULL),(7,'Entertainment',NULL,NULL),(8,'Politics',NULL,NULL);
/*!40000 ALTER TABLE `wiki_article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_article_likes`
--

DROP TABLE IF EXISTS `wiki_article_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_article_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_article_likes`
--

LOCK TABLES `wiki_article_likes` WRITE;
/*!40000 ALTER TABLE `wiki_article_likes` DISABLE KEYS */;
INSERT INTO `wiki_article_likes` VALUES (1,1,16,'2018-06-26 01:28:25','2018-06-26 01:28:25'),(2,1,16,'2018-06-26 01:29:02','2018-06-26 01:29:02'),(3,1,16,'2018-06-26 01:45:38','2018-06-26 01:45:38'),(4,1,16,'2018-06-26 01:46:26','2018-06-26 01:46:26');
/*!40000 ALTER TABLE `wiki_article_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_article_type`
--

DROP TABLE IF EXISTS `wiki_article_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_article_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_article_type`
--

LOCK TABLES `wiki_article_type` WRITE;
/*!40000 ALTER TABLE `wiki_article_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_article_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_articles`
--

DROP TABLE IF EXISTS `wiki_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reporter_user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_type` int(11) NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` tinyint(4) NOT NULL,
  `article_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_photo` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_articles`
--

LOCK TABLES `wiki_articles` WRITE;
/*!40000 ALTER TABLE `wiki_articles` DISABLE KEYS */;
INSERT INTO `wiki_articles` VALUES (1,23,'Loream Ipsum',1,'Loream Ipsum',1,'23/download.jpeg',NULL,'2018-06-18 05:48:57','2018-06-18 05:48:57'),(2,23,'Loream Ipsum 1',1,'Loream Ipsum 1',1,'23/KPJ_n_1.png',NULL,'2018-06-19 00:21:07','2018-06-19 00:21:07'),(3,23,'Loream Ipsum 12',1,'Loream Ipsum 12',1,'23/KPJ_n_1.png',NULL,'2018-06-19 00:49:37','2018-06-19 00:49:37'),(4,23,'Loream Ipsum 123',1,'Loream Ipsum 123',1,'23/KPJ_n_1.png',NULL,'2018-06-19 00:51:31','2018-06-19 00:51:31'),(5,23,'Loream Ipsum 1234',1,'Loream Ipsum 1234',1,'23/KPJ_n_1.png',NULL,'2018-06-19 00:51:57','2018-06-19 00:51:57'),(6,23,'Loream Ipsum 1234',1,'Loream Ipsum 1234',1,'23/KPJ_n_1.png',NULL,'2018-06-19 06:55:28','2018-06-19 06:55:28'),(7,23,'My new article',1,'My new article My new article My new article',2,'23/5939292-images-download.gif',NULL,'2018-06-21 02:19:26','2018-06-21 02:19:26'),(8,23,'My new article',1,'My new article My new article My new article',2,'23/5939292-images-download.gif',NULL,'2018-06-27 00:10:54','2018-06-27 00:10:54'),(9,36,'Test Article',1,'My new article My Test Article article My new article Test Article Test Article',2,'36/Superman2.jpeg',NULL,'2018-06-27 01:26:01','2018-06-27 01:26:01'),(10,36,'Test Article',1,'My new article My Test Article article My new article Test Article Test Article',2,'36/Superman2.jpeg',NULL,'2018-06-27 01:28:46','2018-06-27 01:28:46'),(11,36,'Test Article',1,'My new article My Test Article article My new article Test Article Test Article',2,'36/Superman2.jpeg',NULL,'2018-06-27 01:29:55','2018-06-27 01:29:55');
/*!40000 ALTER TABLE `wiki_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_comment_reply`
--

DROP TABLE IF EXISTS `wiki_comment_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_comment_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reply_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_comment_reply`
--

LOCK TABLES `wiki_comment_reply` WRITE;
/*!40000 ALTER TABLE `wiki_comment_reply` DISABLE KEYS */;
INSERT INTO `wiki_comment_reply` VALUES (1,'Test Reply 2ss','2018-06-27 07:45:22','2018-06-27 07:45:22'),(2,'Test Reply 2ss','2018-06-27 07:46:08','2018-06-27 07:46:08');
/*!40000 ALTER TABLE `wiki_comment_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_comments`
--

DROP TABLE IF EXISTS `wiki_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `comment_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commented_by` int(11) NOT NULL,
  `reply_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_comments`
--

LOCK TABLES `wiki_comments` WRITE;
/*!40000 ALTER TABLE `wiki_comments` DISABLE KEYS */;
INSERT INTO `wiki_comments` VALUES (1,1,'Loream Ipsum',24,NULL,'2018-06-18 07:48:56','2018-06-18 07:48:56'),(2,1,'Loream Ipsum 2',24,NULL,'2018-06-18 23:21:00','2018-06-18 23:21:00'),(3,1,'Loream Ipsum 3',24,NULL,'2018-06-18 23:26:13','2018-06-18 23:26:13'),(4,1,'Loream Ipsum 3',24,NULL,'2018-06-18 23:26:31','2018-06-18 23:26:31'),(5,1,'Loream Ipsum 4',24,NULL,'2018-06-18 23:51:11','2018-06-18 23:51:11'),(6,1,'Loream Ipsum 4',24,NULL,'2018-06-19 00:00:18','2018-06-19 00:00:18'),(7,1,'Loream Ipsum 4',24,NULL,'2018-06-19 00:00:34','2018-06-19 00:00:34'),(8,1,'Loream Ipsum 4',24,NULL,'2018-06-21 00:20:08','2018-06-21 00:20:08'),(9,1,'My new Comment',24,NULL,'2018-06-21 02:09:15','2018-06-21 02:09:15'),(10,1,'Test comment',24,NULL,'2018-06-21 02:09:55','2018-06-21 02:09:55'),(11,7,'Comment on article 7',24,NULL,'2018-06-21 02:23:19','2018-06-21 02:23:19'),(12,7,'Test Reply',36,NULL,'2018-06-27 05:15:45','2018-06-27 05:15:45'),(13,7,'Test Reply 1',36,NULL,'2018-06-27 05:23:00','2018-06-27 05:23:00'),(14,7,'Test Reply 2',36,NULL,'2018-06-27 05:35:21','2018-06-27 05:35:21'),(15,7,NULL,36,2,'2018-06-27 07:46:08','2018-06-27 07:46:08');
/*!40000 ALTER TABLE `wiki_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_data_rows`
--

DROP TABLE IF EXISTS `wiki_data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `wiki_data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_data_rows`
--

LOCK TABLES `wiki_data_rows` WRITE;
/*!40000 ALTER TABLE `wiki_data_rows` DISABLE KEYS */;
INSERT INTO `wiki_data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'',1),(2,1,'name','text','Name',1,1,1,1,1,1,'',2),(3,1,'email','text','Email',1,1,1,1,1,1,'',3),(4,1,'password','password','Password',1,0,0,1,1,0,'',4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,'',5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,'',6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,'',8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'locale','text','Locale',0,1,1,1,1,0,'',12),(12,1,'settings','hidden','Settings',0,0,0,0,0,0,'',12),(13,2,'id','number','ID',1,0,0,0,0,0,'',1),(14,2,'name','text','Name',1,1,1,1,1,1,'',2),(15,2,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(16,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(17,3,'id','number','ID',1,0,0,0,0,0,'',1),(18,3,'name','text','Name',1,1,1,1,1,1,'',2),(19,3,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(20,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(21,3,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(22,1,'role_id','text','Role',1,1,1,1,1,1,'',9);
/*!40000 ALTER TABLE `wiki_data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_data_types`
--

DROP TABLE IF EXISTS `wiki_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_data_types`
--

LOCK TABLES `wiki_data_types` WRITE;
/*!40000 ALTER TABLE `wiki_data_types` DISABLE KEYS */;
INSERT INTO `wiki_data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,NULL,'2018-06-20 02:27:07','2018-06-20 02:27:07'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-06-20 02:27:07','2018-06-20 02:27:07'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2018-06-20 02:27:07','2018-06-20 02:27:07');
/*!40000 ALTER TABLE `wiki_data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_menu_items`
--

DROP TABLE IF EXISTS `wiki_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `wiki_menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_menu_items`
--

LOCK TABLES `wiki_menu_items` WRITE;
/*!40000 ALTER TABLE `wiki_menu_items` DISABLE KEYS */;
INSERT INTO `wiki_menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2018-06-20 02:27:07','2018-06-20 02:27:07','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2018-06-20 02:27:07','2018-06-20 02:27:07','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2018-06-20 02:27:08','2018-06-20 02:27:08',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,10,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,11,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2018-06-20 02:27:08','2018-06-20 02:27:08','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,13,'2018-06-20 02:27:09','2018-06-20 02:27:09','voyager.hooks',NULL);
/*!40000 ALTER TABLE `wiki_menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_menus`
--

DROP TABLE IF EXISTS `wiki_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_menus`
--

LOCK TABLES `wiki_menus` WRITE;
/*!40000 ALTER TABLE `wiki_menus` DISABLE KEYS */;
INSERT INTO `wiki_menus` VALUES (1,'admin','2018-06-20 02:27:07','2018-06-20 02:27:07');
/*!40000 ALTER TABLE `wiki_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_migrations`
--

DROP TABLE IF EXISTS `wiki_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_migrations`
--

LOCK TABLES `wiki_migrations` WRITE;
/*!40000 ALTER TABLE `wiki_migrations` DISABLE KEYS */;
INSERT INTO `wiki_migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2018_06_11_083602_create_user_profile',1),(9,'2018_06_11_085722_create_countries_table',1),(10,'2018_06_13_061100_create_user_verification_table',2),(11,'2018_06_14_065616_create_articles_table',3),(12,'2018_06_14_071246_create_article_category_table',3),(13,'2018_06_14_072757_create_comments_table',3),(14,'2018_06_14_074613_create_article_type_table',3),(15,'2016_01_01_000000_add_voyager_user_fields',4),(16,'2016_01_01_000000_create_data_types_table',4),(17,'2016_05_19_173453_create_menu_table',4),(18,'2016_10_21_190000_create_roles_table',4),(19,'2016_10_21_190000_create_settings_table',4),(20,'2016_11_30_135954_create_permission_table',4),(21,'2016_11_30_141208_create_permission_role_table',4),(22,'2016_12_26_201236_data_types__add__server_side',4),(23,'2017_01_13_000000_add_route_to_menu_items_table',4),(24,'2017_01_14_005015_create_translations_table',4),(25,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',4),(26,'2017_03_06_000000_add_controller_to_data_types_table',4),(27,'2017_04_21_000000_add_order_to_data_rows_table',4),(28,'2017_07_05_210000_add_policyname_to_data_types_table',4),(29,'2017_08_05_000000_add_group_to_settings_table',4),(30,'2017_11_26_013050_add_user_role_relationship',4),(31,'2017_11_26_015000_create_user_roles_table',4),(32,'2018_03_11_000000_add_user_settings',4),(33,'2018_03_14_000000_add_details_to_data_types_table',4),(34,'2018_03_16_000000_make_settings_value_nullable',4),(35,'2018_06_26_055806_create_article_likes_table',5),(36,'2018_06_27_123541_create_comment_reply_table',6);
/*!40000 ALTER TABLE `wiki_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_oauth_access_tokens`
--

DROP TABLE IF EXISTS `wiki_oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_oauth_access_tokens`
--

LOCK TABLES `wiki_oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `wiki_oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `wiki_oauth_access_tokens` VALUES ('06f60665e4e5570fc73a0785b0f76b5e3e75d0bd99e72750ec5aed32c7ad0c5a29fdff27b7803a44',35,1,'WikiXm','[]',0,'2018-06-26 06:31:43','2018-06-26 06:31:43','2019-06-26 12:01:43'),('0a9ed1f9835f7417bb3e3179ff0c39205c3df0995aa033f32fef84f76df4390e29a1712b0f17b68e',37,1,'WikiXm','[]',0,'2018-06-28 06:11:37','2018-06-28 06:11:37','2019-06-28 11:41:37'),('0ffdb7331dd3784e1a6aad7cea35d973dc5125a6ea7c7b9510b8923f5c2721a304acece757a41e07',10,1,'WikiXm','[]',0,'2018-06-13 00:28:44','2018-06-13 00:28:44','2019-06-13 05:58:44'),('16c7d998c60f37524341dd1d3caad68db4644099ffdd648c5009ed69bcb34f2ce184a5dec239b2da',16,1,'WikiXm','[]',0,'2018-06-13 02:36:46','2018-06-13 02:36:46','2019-06-13 08:06:46'),('1b70ff964837ef6ab634605a18e8db152a6f09e9e87451225576ec1cc0ec36df0a8c37a739155f0b',23,1,'WikiXm','[]',0,'2018-06-18 01:52:17','2018-06-18 01:52:17','2019-06-18 07:22:17'),('25e31cd59884e22098c7bc12f69f7d4a8439da1101f12dc57d30a8e19a38e21e8595c507a5492c52',1,1,'WikiXm','[]',0,'2018-06-12 01:20:06','2018-06-12 01:20:06','2019-06-12 06:50:06'),('2fa1ee34b2aabef312d6942dd29356a97bf15e903165a3ecd9401d91e3382c014a08f21cf6f0fd76',17,1,'WikiXm','[]',0,'2018-06-13 03:10:09','2018-06-13 03:10:09','2019-06-13 08:40:09'),('3109d91d6ce931d85547f44efb7a2852d62ace0ff76958f26d062cab1ba82e9378b4f207fbf57c24',36,1,'WikiXm','[]',0,'2018-06-26 23:22:26','2018-06-26 23:22:26','2019-06-27 04:52:26'),('41e4c433a7d7083157518f4ff37a7552227af89cbb81fd4854e32c1fe4180ced67673a3c17e6d49d',32,1,'WikiXm','[]',0,'2018-06-21 00:41:28','2018-06-21 00:41:28','2019-06-21 06:11:28'),('4724504f85c27c5cdbfa682d0020405fa48bd391066a3569a11631ab1b9e7c4c00285c70c9607eb6',18,1,'WikiXm','[]',0,'2018-06-17 23:02:22','2018-06-17 23:02:22','2019-06-18 04:32:22'),('4a0914d4169fa7105c7686eeaa0706ecf62033c1cafe41ad446a698023e34f4b0eeea185f97050fc',36,1,'WikiXm','[]',0,'2018-06-28 05:56:50','2018-06-28 05:56:50','2019-06-28 11:26:50'),('73da3416e3d6e4e31a2b967906dab522712e30a8b2edb8b1565709f20cae81648a2f9093084f41fe',2,1,'WikiXm','[]',0,'2018-06-12 01:59:00','2018-06-12 01:59:00','2019-06-12 07:29:00'),('7b0e825335ad6b808e91529cbbbba12ea00d614cbf0ff8a2c6155b602a6996386b055e48919e4259',33,1,'WikiXm','[]',0,'2018-06-21 00:43:10','2018-06-21 00:43:10','2019-06-21 06:13:10'),('90abd252e8ee081bc684225c47122ac6f1afb07e3aa43cbe7e9f7cfcaa80447765b0fd36355d8ebf',34,1,'WikiXm','[]',0,'2018-06-21 03:16:26','2018-06-21 03:16:26','2019-06-21 08:46:26'),('966607e452f0adb6d9c0bb73bba245611ec028ce050b288f0f5e96058518f5aa9fc47bd6baf53568',36,1,'WikiXm','[]',0,'2018-06-27 00:29:59','2018-06-27 00:29:59','2019-06-27 05:59:59'),('b0c2540e783d07ac40e5214fbc5e0bcf5eed851d61afea8053b3eff6d7afbc8b1b2f945717303204',16,1,'WikiXm','[]',0,'2018-06-13 01:36:04','2018-06-13 01:36:04','2019-06-13 07:06:04'),('b418e5d5751786672a58605f2b756f414d02d462879ef5dd957d8cf74c285b7ef9aeebf8d763bea9',16,1,'WikiXm','[]',0,'2018-06-26 01:05:04','2018-06-26 01:05:04','2019-06-26 06:35:04'),('e754e785aa563d8e8811de5e290f85223958e79c1d5b80f819d3b33d87eda16f6737d75358be2822',16,1,'WikiXm','[]',0,'2018-06-13 02:36:20','2018-06-13 02:36:20','2019-06-13 08:06:20'),('f7234c54c899cb54e1976f50a24aa45c212a5cc26452fdd6a48ba9a5e1688012d2aa72ed13e0076a',24,1,'WikiXm','[]',0,'2018-06-18 07:00:33','2018-06-18 07:00:33','2019-06-18 12:30:33');
/*!40000 ALTER TABLE `wiki_oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_oauth_access_tokens_bkup`
--

DROP TABLE IF EXISTS `wiki_oauth_access_tokens_bkup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_oauth_access_tokens_bkup` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_oauth_access_tokens_bkup`
--

LOCK TABLES `wiki_oauth_access_tokens_bkup` WRITE;
/*!40000 ALTER TABLE `wiki_oauth_access_tokens_bkup` DISABLE KEYS */;
INSERT INTO `wiki_oauth_access_tokens_bkup` VALUES ('07fcedb1aa4e2794f7a93cb6ebe96bcb7c8b2a945ee0d6d32a6b61dbab1586000ab62629b156b70a',1,1,'WikiXm','[]',0,'2018-06-12 00:57:58','2018-06-12 00:57:58','2019-06-12 06:27:58'),('17a3228daa0251defc640a2f7adaf2ca475417b7f112f35250d0b8dc84260029fd0ddcb928646917',4,1,'WikiXm','[]',0,'2018-06-11 23:41:52','2018-06-11 23:41:52','2019-06-12 05:11:52'),('473123643bff43e22b5cd024c845f5b5df5b9522bb2000c263f256ba47d21373208929e35612c184',18,1,'WikiXm','[]',0,'2018-06-12 00:51:50','2018-06-12 00:51:50','2019-06-12 06:21:50'),('504aa6d5f7517aa25edae04de5278733eb375d5549bfe745c98471172db37afed2109c5f009a3d2e',1,1,'WikiXm','[]',0,'2018-06-12 01:17:40','2018-06-12 01:17:40','2019-06-12 06:47:40'),('5215f735a04a8a6a6030df39575758a1e1aff6e06344c3a849e69e45a6002144dd368bdb04ee37ac',1,1,'WikiXm','[]',0,'2018-06-11 05:08:30','2018-06-11 05:08:30','2019-06-11 10:38:30'),('67b07f32401c92e4f22a5cf34f5d489b8b9d64b037f902fb2205a265ccd83a33aa8b7d38d73ebc18',4,1,'WikiXm','[]',0,'2018-06-12 00:02:12','2018-06-12 00:02:12','2019-06-12 05:32:12'),('a6fab7d75e18c5061d0084d6c4771de3828194445a0b30f4ace42e3a45b67c5607805fe52688ef55',7,1,'WikiXm','[]',0,'2018-06-12 00:10:55','2018-06-12 00:10:55','2019-06-12 05:40:55'),('c0f0c47ff54920240648b34c351d7a8c8cc6e9e55edce346297c494311f8a789b8ed7b52b0d14456',1,1,'WikiXm','[]',0,'2018-06-11 05:00:33','2018-06-11 05:00:33','2019-06-11 10:30:33'),('cb5c647fac0a640019f1ee06362edad31830835c00311066c081db866ccdfc1e94f3637462147527',4,1,'WikiXm','[]',0,'2018-06-11 23:54:49','2018-06-11 23:54:49','2019-06-12 05:24:49'),('cd4f729e23c66a22996b4060352f7dcb6a7da2f47197b79d2de1f48897b076b711e44e5cd1a8eac1',4,1,'WikiXm','[]',0,'2018-06-11 23:57:00','2018-06-11 23:57:00','2019-06-12 05:27:00'),('f499e2fdf6f5677e4c4d0edbd277ee4ec51191f4d1a4dc189287f25c3a32916c290995c109405d3f',1,1,'WikiXm','[]',0,'2018-06-12 00:00:18','2018-06-12 00:00:18','2019-06-12 05:30:18');
/*!40000 ALTER TABLE `wiki_oauth_access_tokens_bkup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_oauth_auth_codes`
--

DROP TABLE IF EXISTS `wiki_oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_oauth_auth_codes`
--

LOCK TABLES `wiki_oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `wiki_oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_oauth_clients`
--

DROP TABLE IF EXISTS `wiki_oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_oauth_clients`
--

LOCK TABLES `wiki_oauth_clients` WRITE;
/*!40000 ALTER TABLE `wiki_oauth_clients` DISABLE KEYS */;
INSERT INTO `wiki_oauth_clients` VALUES (1,NULL,'Client 2','7IQUszu88A4YGxkRjLZUOEaeasLEMKYFQCV0k7xn','http://localhost/Wikixm/auth/callback',1,0,0,'2018-06-11 04:06:08','2018-06-11 04:06:08'),(2,NULL,'Client 3','fzHlx2bVy5PcsAl3dtXMRpLLn54y9teUETcFdDKX','http://localhost',0,1,0,'2018-06-11 04:57:18','2018-06-11 04:57:18');
/*!40000 ALTER TABLE `wiki_oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `wiki_oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_oauth_personal_access_clients`
--

LOCK TABLES `wiki_oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `wiki_oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `wiki_oauth_personal_access_clients` VALUES (1,1,NULL,NULL);
/*!40000 ALTER TABLE `wiki_oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `wiki_oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_oauth_refresh_tokens`
--

LOCK TABLES `wiki_oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `wiki_oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_password_resets`
--

DROP TABLE IF EXISTS `wiki_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_password_resets`
--

LOCK TABLES `wiki_password_resets` WRITE;
/*!40000 ALTER TABLE `wiki_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_permission_role`
--

DROP TABLE IF EXISTS `wiki_permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `wiki_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `wiki_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_permission_role`
--

LOCK TABLES `wiki_permission_role` WRITE;
/*!40000 ALTER TABLE `wiki_permission_role` DISABLE KEYS */;
INSERT INTO `wiki_permission_role` VALUES (1,1),(1,3),(2,1),(2,3),(3,1),(3,3),(4,1),(4,3),(5,1),(5,3),(6,1),(6,3),(7,1),(7,3),(8,1),(8,3),(9,1),(9,3),(10,1),(10,3),(11,1),(11,3),(12,1),(12,3),(13,1),(13,3),(14,1),(14,3),(15,1),(15,3),(16,1),(16,3),(17,1),(17,3),(18,1),(18,3),(19,1),(19,3),(20,1),(20,3),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(26,3);
/*!40000 ALTER TABLE `wiki_permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_permissions`
--

DROP TABLE IF EXISTS `wiki_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_permissions`
--

LOCK TABLES `wiki_permissions` WRITE;
/*!40000 ALTER TABLE `wiki_permissions` DISABLE KEYS */;
INSERT INTO `wiki_permissions` VALUES (1,'browse_admin',NULL,'2018-06-20 02:27:08','2018-06-20 02:27:08'),(2,'browse_bread',NULL,'2018-06-20 02:27:08','2018-06-20 02:27:08'),(3,'browse_database',NULL,'2018-06-20 02:27:08','2018-06-20 02:27:08'),(4,'browse_media',NULL,'2018-06-20 02:27:08','2018-06-20 02:27:08'),(5,'browse_compass',NULL,'2018-06-20 02:27:08','2018-06-20 02:27:08'),(6,'browse_menus','menus','2018-06-20 02:27:08','2018-06-20 02:27:08'),(7,'read_menus','menus','2018-06-20 02:27:08','2018-06-20 02:27:08'),(8,'edit_menus','menus','2018-06-20 02:27:08','2018-06-20 02:27:08'),(9,'add_menus','menus','2018-06-20 02:27:08','2018-06-20 02:27:08'),(10,'delete_menus','menus','2018-06-20 02:27:08','2018-06-20 02:27:08'),(11,'browse_roles','roles','2018-06-20 02:27:08','2018-06-20 02:27:08'),(12,'read_roles','roles','2018-06-20 02:27:08','2018-06-20 02:27:08'),(13,'edit_roles','roles','2018-06-20 02:27:08','2018-06-20 02:27:08'),(14,'add_roles','roles','2018-06-20 02:27:08','2018-06-20 02:27:08'),(15,'delete_roles','roles','2018-06-20 02:27:08','2018-06-20 02:27:08'),(16,'browse_users','users','2018-06-20 02:27:08','2018-06-20 02:27:08'),(17,'read_users','users','2018-06-20 02:27:08','2018-06-20 02:27:08'),(18,'edit_users','users','2018-06-20 02:27:08','2018-06-20 02:27:08'),(19,'add_users','users','2018-06-20 02:27:08','2018-06-20 02:27:08'),(20,'delete_users','users','2018-06-20 02:27:08','2018-06-20 02:27:08'),(21,'browse_settings','settings','2018-06-20 02:27:08','2018-06-20 02:27:08'),(22,'read_settings','settings','2018-06-20 02:27:08','2018-06-20 02:27:08'),(23,'edit_settings','settings','2018-06-20 02:27:08','2018-06-20 02:27:08'),(24,'add_settings','settings','2018-06-20 02:27:08','2018-06-20 02:27:08'),(25,'delete_settings','settings','2018-06-20 02:27:08','2018-06-20 02:27:08'),(26,'browse_hooks',NULL,'2018-06-20 02:27:09','2018-06-20 02:27:09');
/*!40000 ALTER TABLE `wiki_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_roles`
--

DROP TABLE IF EXISTS `wiki_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_roles`
--

LOCK TABLES `wiki_roles` WRITE;
/*!40000 ALTER TABLE `wiki_roles` DISABLE KEYS */;
INSERT INTO `wiki_roles` VALUES (1,'admin','Administrator','2018-06-20 02:27:08','2018-06-20 02:27:08'),(2,'user','Normal User','2018-06-20 02:27:08','2018-06-20 02:27:08'),(3,'reporter','Reporter','2018-06-20 02:48:45','2018-06-20 02:48:45');
/*!40000 ALTER TABLE `wiki_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_settings`
--

DROP TABLE IF EXISTS `wiki_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_settings`
--

LOCK TABLES `wiki_settings` WRITE;
/*!40000 ALTER TABLE `wiki_settings` DISABLE KEYS */;
INSERT INTO `wiki_settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','settings/June2018/gXSvuzGY8wYBhOdkEp0z.jpeg','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');
/*!40000 ALTER TABLE `wiki_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_translations`
--

DROP TABLE IF EXISTS `wiki_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_translations`
--

LOCK TABLES `wiki_translations` WRITE;
/*!40000 ALTER TABLE `wiki_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_user_profile`
--

DROP TABLE IF EXISTS `wiki_user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_user_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personal_history` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likes_dislikes` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likes_dislikes_about_town` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `goals` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_user_profile`
--

LOCK TABLES `wiki_user_profile` WRITE;
/*!40000 ALTER TABLE `wiki_user_profile` DISABLE KEYS */;
INSERT INTO `wiki_user_profile` VALUES (1,1,'1991-12-26',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-12 00:57:58','2018-06-12 01:55:27'),(2,2,'1991-12-26',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-12 01:59:00','2018-06-12 02:00:57'),(3,3,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-12 07:51:02','2018-06-12 07:51:02'),(4,4,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-12 07:51:31','2018-06-12 07:51:31'),(5,5,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-12 07:52:36','2018-06-12 07:52:36'),(6,6,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-12 07:53:23','2018-06-12 07:53:23'),(7,7,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 00:25:06','2018-06-13 00:25:06'),(8,8,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 00:26:43','2018-06-13 00:26:43'),(9,9,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 00:27:23','2018-06-13 00:27:23'),(10,10,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 00:28:43','2018-06-13 00:28:43'),(11,11,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 01:32:01','2018-06-13 01:32:01'),(12,12,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 01:33:20','2018-06-13 01:33:20'),(13,14,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 01:35:15','2018-06-13 01:35:15'),(14,16,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 01:36:02','2018-06-13 01:36:02'),(15,17,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-13 03:10:07','2018-06-13 03:10:07'),(16,18,'1992-12-20',NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-17 23:02:20','2018-06-17 23:02:20'),(17,23,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-18 01:52:17','2018-06-18 01:52:17'),(18,24,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-18 07:00:32','2018-06-18 07:00:32'),(19,30,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-21 00:39:20','2018-06-21 00:39:20'),(20,31,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-21 00:40:56','2018-06-21 00:40:56'),(21,32,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-21 00:41:27','2018-06-21 00:41:27'),(22,33,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-21 00:43:09','2018-06-21 00:43:09'),(23,34,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-21 03:16:25','2018-06-21 03:16:25'),(24,35,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-26 06:31:43','2018-06-26 06:31:43'),(25,36,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-26 23:22:25','2018-06-26 23:22:25'),(26,37,'1992-12-20','Male',1,NULL,NULL,NULL,NULL,NULL,'2018-06-28 06:11:36','2018-06-28 06:11:36');
/*!40000 ALTER TABLE `wiki_user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_user_roles`
--

DROP TABLE IF EXISTS `wiki_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `wiki_roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `wiki_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_user_roles`
--

LOCK TABLES `wiki_user_roles` WRITE;
/*!40000 ALTER TABLE `wiki_user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_user_verification`
--

DROP TABLE IF EXISTS `wiki_user_verification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_user_verification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `otp` int(11) NOT NULL,
  `is_otp_verified` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_user_verification`
--

LOCK TABLES `wiki_user_verification` WRITE;
/*!40000 ALTER TABLE `wiki_user_verification` DISABLE KEYS */;
INSERT INTO `wiki_user_verification` VALUES (1,16,9627,'1','2018-06-13 01:36:02','2018-06-13 02:12:58'),(2,17,3160,'1','2018-06-13 03:10:07','2018-06-13 03:13:32'),(3,18,4080,'0','2018-06-17 23:02:20','2018-06-17 23:02:20'),(4,23,6615,'0','2018-06-18 01:52:17','2018-06-18 01:52:17'),(5,24,9378,'0','2018-06-18 07:00:32','2018-06-18 07:00:32'),(6,30,8820,'0','2018-06-21 00:39:20','2018-06-21 00:39:20'),(7,31,2351,'0','2018-06-21 00:40:56','2018-06-21 00:40:56'),(8,32,8090,'0','2018-06-21 00:41:27','2018-06-21 00:41:27'),(9,33,6196,'0','2018-06-21 00:43:09','2018-06-21 00:43:09'),(10,34,6598,'0','2018-06-21 03:16:25','2018-06-21 03:16:25'),(11,35,7383,'1','2018-06-26 06:31:43','2018-06-26 06:32:47'),(12,36,5096,'1','2018-06-26 23:22:25','2018-06-26 23:22:25'),(13,37,4632,'0','2018-06-28 06:11:36','2018-06-28 06:11:36');
/*!40000 ALTER TABLE `wiki_user_verification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wiki_users`
--

DROP TABLE IF EXISTS `wiki_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `wiki_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wiki_users`
--

LOCK TABLES `wiki_users` WRITE;
/*!40000 ALTER TABLE `wiki_users` DISABLE KEYS */;
INSERT INTO `wiki_users` VALUES (1,1,'Rajat Verma','rvrajat084@gmail.com','users/default.png','$2y$10$JdhOIJW6kJpTTjhzz5MsS.4UIMg9nh4GN26ctQvla4VF3.gAgzn8a','8bzi3AI8pMqbvsJE0exXp9tOpsx9LUJGxubvUKfGy1FGRATnW4QrHreFcmqz',NULL,'2018-06-12 00:57:58','2018-06-20 02:45:33'),(2,NULL,'Ajay Kumar','ajay@grr.la','users/default.png','$2y$10$Ts6HFkgi1GG3Fgspi3CdbObUBiGVuYHOpI.OkKeBaqJKoKYPXY0ym',NULL,NULL,'2018-06-12 01:59:00','2018-06-12 01:59:00'),(3,NULL,'Ajay Kumar','test@grr.la','users/default.png','$2y$10$xco1DFEEIIkG48fsz35RVuXQmg1bvA1lrLgCmynEqnQTnyV5FK7NC',NULL,NULL,'2018-06-12 07:51:02','2018-06-12 07:51:02'),(4,NULL,'Ajay Kumar','test1@grr.la','users/default.png','$2y$10$4tKeEewEpsadhnU/5aXXi.NoLziZ4f0keT6hhiqX5cy5GSHJv4ne6',NULL,NULL,'2018-06-12 07:51:31','2018-06-12 07:51:31'),(5,NULL,'Ajay Kumar','test2@grr.la','users/default.png','$2y$10$yzXAZYSjVQtnjsihSQb/JOW4S0hgWWQ75ZWmySFLLxzMqNPwn6MTu',NULL,NULL,'2018-06-12 07:52:36','2018-06-12 07:52:36'),(6,NULL,'Ajay Kumar','test3@grr.la','users/default.png','$2y$10$nj242tof/bVt2J9tzVZVN.diKtGZoQ2YIYvI6W4DihpDUdHxmpsXC',NULL,NULL,'2018-06-12 07:53:23','2018-06-12 07:53:23'),(7,NULL,'Test Four','test4@grr.la','users/default.png','$2y$10$g8beCbWFC8OQUDbOZxiJJutPnZANSkK.BztZrd.s98ueC4tyxGul2',NULL,NULL,'2018-06-13 00:25:06','2018-06-13 00:25:06'),(8,NULL,'Test Four','test5@grr.la','users/default.png','$2y$10$97pTsZUKUJ1VBpPOqDmc/OVBecmBEIXLNaO4/vSS/MPPx0qdGIXLS',NULL,NULL,'2018-06-13 00:26:43','2018-06-13 00:26:43'),(9,NULL,'Test Four','test6@grr.la','users/default.png','$2y$10$prqLKrXJe1OtrMMY0tLz.uiRDhdW2jRt7.0xgIt84s.bkCC0oTvne',NULL,NULL,'2018-06-13 00:27:23','2018-06-13 00:27:23'),(10,NULL,'Test Four','test7@grr.la','users/default.png','$2y$10$F9yaHX8ns8JHkyW8INSzt.5aCOEOqiogA72yoJwOByjxCEnLKndNO',NULL,NULL,'2018-06-13 00:28:43','2018-06-13 00:28:43'),(11,NULL,'Test Eight','test8@grr.la','users/default.png','$2y$10$cqP.AC.sVvo9M5HZATzUG.CHa1aAbgG6LhhbR2NcamTqIzMaRo73.',NULL,NULL,'2018-06-13 01:32:01','2018-06-13 01:32:01'),(12,NULL,'Test Nine','test9@grr.la','users/default.png','$2y$10$hs/U8dK3odAQKu2z390hOeK5BHPNyx8wrs3/6mO2EMKD1jKwPWekO',NULL,NULL,'2018-06-13 01:33:20','2018-06-13 01:33:20'),(14,NULL,'Test Ten','test10@grr.la','users/default.png','$2y$10$RhlFmkMsvSGCXTEJ6D.vruXUHr9Mx1A3dDzD1ziCXoHbIHS2XmdW6',NULL,NULL,'2018-06-13 01:35:15','2018-06-13 01:35:15'),(16,NULL,'Test Ten','test11@grr.la','users/default.png','$2y$10$gnhFwaRznwxXLnGS0cq67eln22cl.pEOD4ypcKE1QDI8hNM5fInJS',NULL,NULL,'2018-06-13 01:36:02','2018-06-13 01:36:02'),(17,NULL,'Test Twelve','test12@grr.la','users/default.png','$2y$10$6PHFubLxtj2kLf3Y65MNK.rTGJ.Ui26o4Tup1.rpJT63pltwTu0WO',NULL,NULL,'2018-06-13 03:10:07','2018-06-13 03:10:07'),(18,NULL,'Test Twelve','test13@grr.la','users/default.png','$2y$10$0FIEhmeeQZWbytHemmsbg.f21Wkdc2zwj.1oZujOyg0tXeH/q/.cq',NULL,NULL,'2018-06-17 23:02:20','2018-06-17 23:02:20'),(19,NULL,'Test Twelve','test14@grr.la','users/default.png','$2y$10$2lWmmOJX2Dxg0kg5Cp54bOyZU9FyS87IN2n9uqv3K3oc6m7kre8JC',NULL,NULL,'2018-06-18 01:46:23','2018-06-18 01:46:23'),(21,NULL,'Test Twelve','test15@grr.la','users/default.png','$2y$10$AC3cWhMxPBmvBVGmUvXCKOY65PUXy49ABkOn82FW7m93BIpB/c6sC',NULL,NULL,'2018-06-18 01:50:32','2018-06-18 01:50:32'),(22,NULL,'Test Twelve','test16@grr.la','users/default.png','$2y$10$eTxQRqRgx.vCj6XsFft3Sug2lD8P/Nhc8JqarCKui5PtjFEyMl52i',NULL,NULL,'2018-06-18 01:51:59','2018-06-18 01:51:59'),(23,NULL,'Test Twelve','test17@grr.la','users/default.png','$2y$10$UhT3/hcNySa0zUrkuK.LyOahUNMfm/z.RLdKDG.rq2yeMTu7OA96a',NULL,NULL,'2018-06-18 01:52:17','2018-06-18 01:52:17'),(24,NULL,'Test Twelve','test18@grr.la','users/default.png','$2y$10$KkU3GNzsbRJki/TYuUvBUOrwj8eHkQy2YdsgsiqciTMDQ7MHp3C5O',NULL,NULL,'2018-06-18 07:00:32','2018-06-18 07:00:32'),(25,2,'Test','testing@grr.la','users/default.png','$2y$10$uceX3TTdaB705vdV17mz7OP3TlxuBmYRPRtZhiX/7p7ev4HMxVsJW',NULL,NULL,'2018-06-20 03:15:35','2018-06-20 03:15:35'),(26,2,'Nineteen','test19@grr.la','users/default.png','$2y$10$oo0ztIVERBrWFQUoyWnQteoH6g/7K2lCkEuyhIvJdgpp0d6NdtNuO',NULL,NULL,'2018-06-21 00:38:05','2018-06-21 00:38:05'),(28,2,'Nineteen','test20@grr.la','users/default.png','$2y$10$T7uMuUQJiouBgzwHr8qFmuUi.ky3LWz0UO3b0C0kTvWJ73ztOf38C',NULL,NULL,'2018-06-21 00:38:50','2018-06-21 00:38:50'),(30,2,'Nineteen','test21@grr.la','users/default.png','$2y$10$PW.PuP1ot4/9dJEBO5gExOTJV9jdK/7sY6uScA5AuTvjLaQO0olTy',NULL,NULL,'2018-06-21 00:39:20','2018-06-21 00:39:20'),(31,2,'Nineteen','test22@grr.la','users/default.png','$2y$10$jUzZ7Vr9G2hpkBx0sAo6z.izlqvQilk5ATljdtMZi0p7sMb9jvO5W',NULL,NULL,'2018-06-21 00:40:56','2018-06-21 00:40:56'),(32,2,'Nineteen','test23@grr.la','users/default.png','$2y$10$Bqx03v8bp7ydVNjPVwMxauXWzKjdnfNiumdCdhDbqyO0TPf3fgc6q',NULL,NULL,'2018-06-21 00:41:27','2018-06-21 00:41:27'),(33,2,'Nineteen','test25@grr.la','users/default.png','$2y$10$yx.XuYZ9nxb6WkouigUyu.J2A78bLBbTYV5xo9DyJcPA2Grd8AO52',NULL,NULL,'2018-06-21 00:43:09','2018-06-21 00:43:09'),(34,2,'Tweny Six','test26@grr.la','users/default.png','$2y$10$yx1861FH1iCLKmWbJ572bOzFwBiDMjYKDskQVwEiDv7ApHOXETDT.',NULL,NULL,'2018-06-21 03:16:25','2018-06-21 03:16:25'),(35,2,'Rajat Test','rajattest@grr.la','users/default.png','$2y$10$3NX.JJXc/2yGx5Tyzlz12.uIRnXyBAB0rPR8Wgukkp9KrFkM9Ra4m',NULL,NULL,'2018-06-26 06:31:43','2018-06-26 06:31:43'),(36,3,'Test Reporter','newreporter@grr.la','users/default.png','$2y$10$l/E1tCOwIN5QlDctx4SdNu7H/EixrjRbHWpxdt8.c6klsuRaVIPvO',NULL,NULL,'2018-06-26 23:22:25','2018-06-26 23:22:25'),(37,2,'Test Reporter111','newreporter1@grr.la','users/default.png','$2y$10$4rj/FB0G.H3do41YRs7STO/WQqmvczhXbaDdJjAGRVY27xDJKFf6S',NULL,NULL,'2018-06-28 06:11:36','2018-06-28 06:11:36');
/*!40000 ALTER TABLE `wiki_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-29 10:46:27
