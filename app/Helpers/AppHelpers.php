<?php

namespace App\Helpers;

use Illuminate\Http\Request;

Class AppHelper
{
	public static function sendMail($email)
	{
		
       try {
       	    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

            $response = $sendgrid->send($email);

            return response()->json(['status'=> $response->statusCode()], 200);

            } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
	}
}