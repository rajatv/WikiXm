<?php

namespace App\Helpers;

use Illuminate\Http\Request;

Class AppHelper
{
  public static function sendMail($email)
  {

       try {
            $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

            $response = $sendgrid->send($email);
            return $response->statusCode();

            } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
  }

  public static function setEmailAttributes($substitution,$emailID,$templateID)
    {
        $email = new \SendGrid\Mail\Mail(); 
        $email->setTemplateId($templateID);
        $email->addSubstitutions($substitution);
        $email->setFrom("test@example.com", "Admin User");
        $email->addTo($emailID, "Example User");
        
        $response = AppHelper::sendMail($email);
        
        return response()->json(['status' => $response,'msg' => 'Email send successfully'], 200);
        
    }

    public static function getUserNameById($id)
    {
      User::where(['id' => $id])->first();
    }
}