<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function createUserToken($user)
    {
      return $user->createToken('WikiXm')->accessToken;
    }

    public function userProfile()
    {
        return $this->hasOne('App\Models\UserProfile');
    }

    public function userVerification()
    {
        return $this->hasOne('App\Models\UserVerification');
    }

    public function userRole()
    {
        return $this->belongsTo('App\Models\Role','role_id','id');
    }
}
