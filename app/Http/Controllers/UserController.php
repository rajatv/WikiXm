<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\UserProfile;
use Auth;
use App\Helpers\AppHelper;
use AppConstants;
use App\Models\UserVerification;

class UserController extends Controller
{
	public $successStatus = 200;
	public $failureStatus = 401;

    public function registerUser(Request $request)
    {
    	$input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        
        // creating user
        $user = User::create($input); 
        
        //creating user profile
        $usreProfileData = array();
        $userProfileData['date_of_birth']  = $input['date_of_birth'];
        $userProfileData['gender']  = $input['gender'];
        
        $userProfileObj = new UserProfile;
        $userProfileObj->user_id = $user->id;
        
        $userProfileObj->date_of_birth = $userProfileData['date_of_birth'];
        $userProfileObj->country_id = 1;
        $userProfileObj->gender = $userProfileData['gender'];
        $userProfileObj->save();

        //creating user otp
        $otp = $this->createUserOtp();
        $userVerificationObj = new UserVerification;
        $userVerificationObj->user_id = $user->id;
        $userVerificationObj->otp = $otp;
        $userVerificationObj->save();

       // Sending registration email
        $substitution = array('-user-' => $input['name'],'-otp-' => $otp);

        AppHelper::setEmailAttributes($substitution,$input['email'],AppConstants::REGISTRATION_MAIL_TEMPLATE);

        // Createing user token
        $success['token'] =  User::createUserToken($user);
        $success['msg'] =  "User Created Successfully";

        return response()->json(['success'=>$success], $this-> successStatus); 
    }

    public function login(Request $request)
    {
    	if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user()->userVerification; 
            if($user->is_otp_verified)
            {
            $success['token'] =  Auth::user()->createToken('WikiXm')-> accessToken;
            $success['msg'] = "Login Succcessfully";
            $success['status'] = "200";
            return response()->json(['msg' => $success], $this->successStatus); 
            }else{
            return response()->json(['msg' => 'Please verify otp to login.'], $this->failureStatus); 	
            }
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function updateProfileInfo(Request $request)
    {
    	$user = User::find(Auth::user()->id)->userProfile;
    	$user->first_name = $request->first_name;
    	$user->last_name = $request->last_name;
    	$user->date_of_birth = $request->date_of_birth;
    	$user->save();
    	return response()->json(['status' => 'Profile Updated Successfully'], 200);
    }

    public function createUserOtp()
    {
    	$digits = 4;
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    public function verifyUserOtp(Request $request)
    {
       $userVerification = UserVerification::where(['otp' => $request->otp])->first();
       if($userVerification)
       {
       	 $userVerification->is_otp_verified = '1';
       	 $userVerification->save();

         return response()->json(['status' => 'Your registration is successfully completed'], 200);
       }
    }

    
}

