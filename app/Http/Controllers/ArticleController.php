<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comments;
use App\Models\ArticleLikes;
use App\Models\CommentsReply;
use Auth;

class ArticleController extends Controller
{
	public $successStatus = 200;
	public $failureStatus = 401;

    public function addNewArticle(Request $request)
    {
       $input   = $request->all(); 
       $input['reporter_user_id'] = Auth::user()->id;
       $article = Article::create($input);
       $path = $request->file('article_photo')->storeAs(
               Auth::user()->id, $request->file('article_photo')->getClientOriginalName());
       $article->article_photo = $path;
       $article->save();

       $articleCategory = Article::getArticleCategory($article->category);
       $article->category = $articleCategory;
 
       $status['data'] = $article;
       $status['msg'] = 'Article created successfully';
       
       return response()->json(['status' => $status], $this->successStatus); 
    }

    public function addComments(Request $request)
    {
    	// Added comments to article.
    	  $input    = $request->all(); 
        $input['commented_by'] = Auth::user()->id;  
        $comments = Comments::create($input);

        // All comments in that article in response 
        $articleComments = Comments::where(['article_id' => $input['article_id']])->orderBy('id','asc')->get();
        
        $status['msg'] = 'Comment added successfully';
        $status['comments'] = $articleComments;
        return response()->json(['status' => $status],$this-> successStatus);
    }

    public function getArticleComments($article_id)
    {
      // Fetching articles with comments
      $articleComments = Article::with(['comments.user'])->where(['id' => $article_id])->get();

      $articleData = array();
      foreach($articleComments as $articleComment)
      {
      // article  
      $article['id'] = $articleComment->id;
      $article['title'] = $articleComment->title;
      $article['description'] = $articleComment->description;
      $commentsCount = $articleComment->comments->count();
      $article['total_comment'] = $commentsCount;

      $articleCommentsData = array();
      
      foreach($articleComment->comments as $ac)
      {
      // comments with commentee
      $comments['id'] = $ac['id'];
      $comments['comments'] = $ac['comment_description'];
      $comments['commented_by'] = $ac['user']['name'];
      $comments['comments'] = $ac['user']['name'];
      $article['comments'][] = $comments;
      }
      $articleData[]= $article;
      }

      return response()->json(['status' => 'Success','data' => $articleData],$this-> successStatus);
  }


  public function getArticles()
  {
    $articles = Article::with(['reporter','articleCategory', 'comments'])->get(); 
    $articleFilter = $articles->filter(function($value, $key) {
    if (isset($value->reporter)) 
      {
        return true;
      }
   });
 
    $articleFilter->all();
    $articleData = array();
    foreach($articleFilter as $filter)
    {
       $article['id'] = $filter['id'];
       $article['title'] = $filter['title'];
       $article['description'] = $filter['description'];
       $article['category'] = $filter->articleCategory->name;
       $article['reporter'] = $filter['reporter']->name;
       $article['updated_at'] = $filter['updated_at'];
       
       $data = [];
       foreach ($filter->comments as $articleComment) 
       {
        $comments['comment'] = $articleComment->comment_description;
        $data[] = $comments;
       }
       $article ['comments'] = $data;
       $articleData[] = $article;
    }

    return response()->json(['status' => 'Success','data' => $articleData],$this-> successStatus);
  }

  public function addArticleLikes($article_id)
  { 
     $input['article_id'] = $article_id;
     $input['user_id']    = Auth::user()->id;
     
     ArticleLikes::create($input);

     $totalLikes = ArticleLikes::all()->count();
     $data['totalLikes'] = $totalLikes;
     
     return response()->json(['status' => 'Success','data' => $data],$this-> successStatus);
  }    

  public function addCommentReply(Request $request)
  {   

    $commentReply = new CommentsReply;
    $commentReply->reply_description = $request->reply_description;
    $commentReply->save();

     $articleComment = new Comments;

     $articleComment->article_id = $request->article_id;
     $articleComment->commented_by = Auth::user()->id;
     $articleComment->reply_id = $commentReply->id;
     $articleComment->save();

     $data['reply_id'] =  $articleComment->id;
     $data['comment_description'] =  $articleComment->comment_description;
     
     return response()->json(['status' => 'Reply added successfully','data' => $data],$this-> successStatus);
  }

  public function getArticleWithComments()
  {
    $articleComments = Comments::where('article_id',7)->get();

    foreach($articleComments as $articleComment)
    {
      if(!empty($articleComment))
      {
        // $article['parent'] = 
      }

    }

    dd($articleComments);
  }


}
