<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;


class TokenController extends Controller
{ 

	public function login(Request $request)
	{
       $this->createToken($request->email);
	}

    public function createToken(Request $request)
    {
      $user = User::where(['email' => $request->email])->first();
      if($user){
      	return response()->json([
               'status' => '200',
               'msg'    => 'OK',
               'token'  =>  User::createUserToken($user)
      		]);
      }else{
      	return response()->json([
             'status' => 401,
             'msg'    => "User with this email id not found"
      		]);
      }
    }
}
