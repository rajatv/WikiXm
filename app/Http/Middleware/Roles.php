<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class Roles
{
    public $successStatus = 200;
    public $failureStatus = 401;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if(!Auth::check())
        {
            return response()->json(['status' => 'User not logged in.'], $this->failureStatus); 
        }
        if(!(Auth::user()->userRole->name == 'reporter'))
        {
            return response()->json(['status' => 'Only reporters can make Article.'], $this->failureStatus); 
        }
        
        return $next($request);
    }
}
