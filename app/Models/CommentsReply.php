<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentsReply extends Model
{
    protected $table = 'comment_reply';

    public function commentReply()
    {
    	return $this->belongsTo('App\Models\Comments','reply_id');
    }
}
