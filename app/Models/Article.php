<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['reporter_user_id', 'title', 'article_type','description','category','article'];

    public function comments()
    {
        return $this->hasMany('App\Models\Comments','article_id');
    }

    public function reporter()
    {
    	return $this->belongsTo('App\User','reporter_user_id');
    }

    public function articleCategory()
    {
    	return $this->belongsTo('App\Models\ArticleCategory','category');
    }

    public static function getArticleCategory($categoryId)
    {
      	$category = \App\Models\ArticleCategory::select('name')->where(['id' => $categoryId])->first();
      	
      	return $category->name;
    } 


}
