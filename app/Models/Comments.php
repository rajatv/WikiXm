<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['article_id', 'comment_description', 'commented_by'];

    public function user()
    {
    	return $this->belongsTo('App\User','commented_by');
    }
}
